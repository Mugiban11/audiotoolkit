﻿using System;
using System.Collections;
using System.Collections.Generic;
using ID.Audio;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class MusicVolumeSlider : MonoBehaviour
{
    private Slider slider;
    public string groupType = "music";

    private void Awake()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(OnValueChange);
    }

    private void OnValueChange(float volumeValue)
    {
        volumeValue = volumeValue.CalculateVolumeValue(slider);
        switch (groupType)
        {
            case "music":
                AudioManager.Instance.SetMusicVolume(volumeValue);
                break;
            case "sfx":
                AudioManager.Instance.SetSFXVolume(volumeValue);
                break;
        }
    }
}
